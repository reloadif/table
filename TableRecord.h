#pragma once

#include <string>

template <typename typeValue>
class TableRecord
{
	std::string key;
	typeValue value;

public:

	TableRecord(const std::string& _key = "NULL", typeValue _value = NULL);
	TableRecord(const TableRecord& tableRecord);

	std::string getKey() const;
	typeValue getValue() const;
	
	void setKey(const std::string& _key);
	void setValue(typeValue _value);

	TableRecord& operator = (const TableRecord& tableRecord);

	bool operator == (const TableRecord& tableRecord);
	bool operator != (const TableRecord& tableRecord);
	bool operator < (const TableRecord& tableRecord);
	bool operator > (const TableRecord& tableRecord);
};


template <typename typeValue>
TableRecord<typeValue>::TableRecord(const std::string& _key, typeValue _value) : key(_key), value(_value) {

}
template <typename typeValue>
TableRecord<typeValue>::TableRecord(const TableRecord& tableRecord) : key(tableRecord.key), value(tableRecord.value) {

}


template <typename typeValue>
std::string TableRecord<typeValue>::getKey() const {
	return key;
}
template <typename typeValue>
typeValue TableRecord<typeValue>::getValue() const {
	return value;
}


template <typename typeValue>
void TableRecord<typeValue>::setKey(const std::string& _key) {
	key = _key;
}
template <typename typeValue>
void TableRecord<typeValue>::setValue(typeValue _value) {
	value = _value;
}


template <typename typeValue>
TableRecord<typeValue>& TableRecord<typeValue>::operator = (const TableRecord& tableRecord) {
	if (this == &tableRecord) {
		return *this;
	}

	key = tableRecord.key;
	value = tableRecord.value;

	return *this;
}


template <typename typeValue>
bool TableRecord<typeValue>::operator == (const TableRecord& tableRecord) {
	return key == tableRecord.key;
}
template <typename typeValue>
bool TableRecord<typeValue>::operator != (const TableRecord& tableRecord) {
	return key != tableRecord.key;
}
template <typename typeValue>
bool TableRecord<typeValue>::operator < (const TableRecord& tableRecord) {
	return key < tableRecord.key;
}
template <typename typeValue>
bool TableRecord<typeValue>::operator > (const TableRecord& tableRecord) {
	return key > tableRecord.key;
}