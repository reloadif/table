#pragma once

// #include <string>

#include "TableRecord.h"

template <typename typeValue> class TreeTable;

template <typename typeValue>
class TreeRecord : public TableRecord<typeValue> {
protected:
	TreeRecord<typeValue>* pointerToLeft;
	TreeRecord<typeValue>* pointerToRight;

public:

	TreeRecord(const std::string& _key = "NULL", typeValue _value = NULL);
	TreeRecord(const TreeRecord& treeRecord);

	TreeRecord<typeValue>* getPointerToLeft() const;
	TreeRecord<typeValue>* getPointerToRight() const;

	void setPointerToLeft(TreeRecord<typeValue>* _pointerToLeft);
	void setPointerToRight(TreeRecord<typeValue>* _pointerToRight);

	TreeRecord& operator = (const TreeRecord& treeRecord);

};

template <typename typeValue>
TreeRecord<typeValue>::TreeRecord(const std::string& _key , typeValue _value ) : TableRecord<typeValue>(_key, _value), pointerToLeft(nullptr), pointerToRight(nullptr) {
	
}
template <typename typeValue>
TreeRecord<typeValue>::TreeRecord(const TreeRecord& treeRecord) : TableRecord<typeValue>(treeRecord.getKey(), treeRecord.getValue()), pointerToLeft(treeRecord.getPointerToLeft()), pointerToRight(treeRecord.getPointerToRight()) {

}


template <typename typeValue>
TreeRecord<typeValue>* TreeRecord<typeValue>::getPointerToLeft() const {
	return pointerToLeft;
}
template <typename typeValue>
TreeRecord<typeValue>* TreeRecord<typeValue>::getPointerToRight() const {
	return pointerToRight;
}

template <typename typeValue>
void TreeRecord<typeValue>::setPointerToLeft(TreeRecord<typeValue>* _pointerToLeft) {
	pointerToLeft = _pointerToLeft;
}
template <typename typeValue>
void TreeRecord<typeValue>::setPointerToRight(TreeRecord<typeValue>* _pointerToRight) {
	pointerToRight = _pointerToRight;
}


template <typename typeValue>
TreeRecord<typeValue>& TreeRecord<typeValue>::operator = (const TreeRecord& treeRecord) {
	if (this == &treeRecord) {
		return *this;
	}

	this->key = treeRecord.key;
	this->value = treeRecord.value;

	pointerToLeft = new TreeRecord(treeRecord.pointerToLeft->getKey(), treeRecord.pointerToLeft->getValue());
	pointerToRight = new TreeRecord(treeRecord.pointerToRight->getKey(), treeRecord.pointerToRight->getValue());

	return *this;
}