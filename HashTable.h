#pragma once

#include <iostream>
#include <iomanip>
// #include <string>
// #include <exception>

#include "AbstractTable.h"
#include "TableRecord.h"
#include "Hash.h"

template <typename typeValue>
class HashTable : public AbstractTable<typeValue, TableRecord>
{
	Hash* hashFunction;
	int identicalRecords;

	int getIndex(std::string _key);

	template <typename typeValue> friend std::ostream& operator << (std::ostream& out, HashTable<typeValue>& hashTable);

public:
	HashTable(const int _sizeOfTable = 10);
	HashTable(const HashTable& hashTable);

	int findRecord(const std::string& _key) override;
	void insertRecord(const std::string& _key, typeValue _value) override;
	void deleteRecord(const std::string& _key) override;
};

template <typename typeValue>
HashTable<typeValue>::HashTable(const int _sizeOfTable) : AbstractTable<typeValue, TableRecord>(_sizeOfTable), hashFunction(&(Hash::getInstance())), identicalRecords(0) {

}
template <typename typeValue>
HashTable<typeValue>::HashTable(const HashTable& hashTable) : AbstractTable<typeValue, TableRecord>(hashTable), hashFunction(&(Hash::getInstance())), identicalRecords(hashTable.identicalRecords) {

}


template <typename typeValue>
int HashTable<typeValue>::getIndex(std::string _key) {
	int index = hashFunction->getControlSum(hashFunction->getHashString(_key));
	index = index % (this->getSizeOfTable()+1);
	return index;
}


template <typename typeValue>
int HashTable<typeValue>::findRecord(const std::string& _key) {
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();
	int index = this->getIndex(_key);

	TableRecord<typeValue> record = recordArray[index];

	if (record.getKey() == _key) {
		return index;
	}
	return -1;
}
template <typename typeValue>
void HashTable<typeValue>::insertRecord(const std::string& _key, typeValue _value) {
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();

	if (findRecord(_key) == -1) {
		recordArray[this->getIndex(_key)] = TableRecord<typeValue>(_key, _value);
		this->numberOfRecords++;
	}
	else {
		this->identicalRecords++;
		recordArray[(this->getIndex(_key) + 3 * identicalRecords) % (this->getSizeOfTable() + 1)] = TableRecord<typeValue>(_key, _value);
		this->numberOfRecords++;
	}
}
template <typename typeValue>
void HashTable<typeValue>::deleteRecord(const std::string& _key) {
	TableRecord<typeValue>* recordArray = this->getArrayOfRecords();

	if (findRecord(_key) != -1) {
		if (identicalRecords) {
			recordArray[(this->getIndex(_key) + 3*identicalRecords) % (this->getSizeOfTable() + 1)] = TableRecord<typeValue>("NULL", NULL);
			this->identicalRecords--;
			this->numberOfRecords--;
		}
		else {
			recordArray[this->getIndex(_key)] = TableRecord<typeValue>("NULL", NULL);
			this->numberOfRecords--;
		}
	}
}


template <typename typeValue>
std::ostream& operator << (std::ostream& out, HashTable<typeValue>& hashTable) {
	TableRecord<typeValue>* recordArray = hashTable.getArrayOfRecords();

	std::cout << "ID |"
		<< std::setw(10) << "Key |"
		<< std::setw(12) << "Value |"
		<< "\t"
		<< std::endl;

	for (int record = 0; record < hashTable.getSizeOfTable(); ++record) {
		TableRecord<typeValue> recording = recordArray[record];

		if (recording.getKey() != "NULL") {
			std::cout << hashTable.getIndex(recording.getKey())
				<< std::setw(10) << recording.getKey()
				<< std::setw(12) << recording.getValue()
				<< "\t"
				<< std::endl;
		}
	}

	return out;
}