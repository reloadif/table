#pragma once

#include <string>

class Hash
{
private:
    std::string hashString;

    Hash();
    Hash(const Hash&) = delete;
    Hash& operator=(Hash&) = delete;

    int receivingExistCode(int code);
public:
    static Hash& getInstance();

    int getControlSum(std::string _string);
    std::string getHashString(std::string userString);
};