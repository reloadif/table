#pragma once
#pragma once

#include <iostream>
#include <iomanip>
#include <queue>
// #include <string>
// #include <exception>

#include "TreeRecord.h"

template <typename typeValue>
class TreeTable
{
	int numberOfRecords;
	int efficiencyIndicator;

	TreeRecord<typeValue>* firstRecord;

	template <typename typeValue> friend std::ostream& operator << (std::ostream& out, TreeTable<typeValue>& treeTable);

	void freeTree(TreeRecord<typeValue>* treeRecord);
public:

	TreeTable();
	TreeTable(const TreeTable& treeTable);

	~TreeTable();

	int getNumberOfRecords() const;
	int getEfficiencyIndicator() const;

	bool isEmpty() const;

	TreeRecord<typeValue>* findRecord(const std::string& _key);
	void insertRecord(const std::string& _key, typeValue _value);
	void deleteRecord(const std::string& _key);
};


template <typename typeValue>
TreeTable<typeValue>::TreeTable() 
: numberOfRecords(0), efficiencyIndicator(0), firstRecord(nullptr) {

}
template <typename typeValue>
TreeTable<typeValue>::TreeTable(const TreeTable& treeTable) 
{
	
}

template <typename typeValue>
void TreeTable<typeValue>::freeTree(TreeRecord<typeValue>* treeRecord) {
	if (treeRecord) {
		if (treeRecord->getPointerToLeft() != nullptr) {
			freeTree(treeRecord->getPointerToLeft());
		}
		if (treeRecord->getPointerToRight() != nullptr) {
			freeTree(treeRecord->getPointerToRight());
		}
		delete treeRecord;
	}
}

template <typename typeValue>
TreeTable<typeValue>::~TreeTable() {
	freeTree(firstRecord);
}

template <typename typeValue>
int TreeTable<typeValue>::getNumberOfRecords() const {
	return numberOfRecords;
}
template <typename typeValue>
int TreeTable<typeValue>::getEfficiencyIndicator() const {
	return efficiencyIndicator;
}

template <typename typeValue>
bool TreeTable<typeValue>::isEmpty() const {
	return firstRecord == nullptr;
}

template <typename typeValue>
TreeRecord<typeValue>* TreeTable<typeValue>::findRecord(const std::string& _key) {
	TreeRecord<typeValue>* currentRecord = firstRecord;

	while (currentRecord != nullptr) {
		this->efficiencyIndicator++;
		if (currentRecord->getKey() == _key)
			return currentRecord;
		else if(currentRecord->getKey() > _key)
			currentRecord = currentRecord->getPointerToLeft();
		else 
			currentRecord = currentRecord->getPointerToRight();
	}

	return currentRecord;
}
template <typename typeValue>
void TreeTable<typeValue>::insertRecord(const std::string& _key, typeValue _value) {
	if( numberOfRecords == 0){
		firstRecord = new TreeRecord<typeValue>(_key, _value);
		numberOfRecords++;
	}
	else {
		TreeRecord<typeValue>* previousRecord = firstRecord;
		TreeRecord<typeValue>* findedRecord = firstRecord;
		bool isLeft = false;

		while (findedRecord != nullptr) {
			this->efficiencyIndicator++;
			if (findedRecord->getKey() == _key)
				break;
			else if (findedRecord->getKey() > _key) {
				previousRecord = findedRecord;
				findedRecord = findedRecord->getPointerToLeft();
				isLeft = true;
			}
			else {
				previousRecord = findedRecord;
				findedRecord = findedRecord->getPointerToRight();
				isLeft = false;
			}
			
		}

		if (findedRecord == nullptr) {
			if (isLeft) {
				previousRecord->setPointerToLeft( new TreeRecord<typeValue>(_key, _value) );
			}
			else previousRecord->setPointerToRight(new TreeRecord<typeValue>(_key, _value));

			numberOfRecords++;
		}
		else {
			findedRecord->setValue(findedRecord->getValue() + _value);
		}
	}
}
template <typename typeValue>
void TreeTable<typeValue>::deleteRecord(const std::string& _key) {
	TreeRecord<typeValue>* previousRecord = firstRecord;
	TreeRecord<typeValue>* findedRecord = firstRecord;
	bool isLeft = false;

	while (findedRecord != nullptr) {
		this->efficiencyIndicator++;
		if (findedRecord->getKey() == _key)
			break;
		else if (findedRecord->getKey() > _key) {
			previousRecord = findedRecord;
			findedRecord = findedRecord->getPointerToLeft();
			isLeft = true;
		}
		else {
			previousRecord = findedRecord;
			findedRecord = findedRecord->getPointerToRight();
			isLeft = false;
		}

	}

	if (findedRecord != nullptr) {
		if (findedRecord->getPointerToLeft() == nullptr && findedRecord->getPointerToRight() == nullptr) {
			delete findedRecord;
			numberOfRecords--;
			if (isLeft) 
				previousRecord->setPointerToLeft(nullptr);
			
			else 
				previousRecord->setPointerToRight(nullptr);
			
		}
		else {
			if (findedRecord->getPointerToLeft() == nullptr && findedRecord->getPointerToRight() != nullptr) {
				numberOfRecords--;
				if (isLeft)
					previousRecord->setPointerToLeft(findedRecord->getPointerToRight());

				else
					previousRecord->setPointerToRight(findedRecord->getPointerToRight());
			}
			else if (findedRecord->getPointerToLeft() != nullptr && findedRecord->getPointerToRight() == nullptr) {
				numberOfRecords--;
				if (isLeft)
					previousRecord->setPointerToLeft(findedRecord->getPointerToLeft());

				else
					previousRecord->setPointerToRight(findedRecord->getPointerToLeft());
			}
			else {
				numberOfRecords--;
				
				TreeRecord<typeValue>* insertionRecord = findedRecord;
				previousRecord = insertionRecord;
				insertionRecord = insertionRecord->getPointerToRight();

				if (insertionRecord->getPointerToLeft() != nullptr) {
					while (insertionRecord->getPointerToLeft() != nullptr) {
						previousRecord = insertionRecord;
						insertionRecord = insertionRecord->getPointerToLeft();
					}
					findedRecord->setKey(insertionRecord->getKey());
					findedRecord->setValue(insertionRecord->getValue());

					delete (previousRecord->getPointerToLeft());
					previousRecord->setPointerToLeft(nullptr);
				}
				else {
					findedRecord->setKey(insertionRecord->getKey());
					findedRecord->setValue(insertionRecord->getValue());

					delete (previousRecord->getPointerToRight());
					previousRecord->setPointerToRight(nullptr);
				}
			}
		}
	}
}


template <typename typeValue>
std::ostream& operator << (std::ostream& out, TreeTable<typeValue>& treeTable) {
	

	std::cout << "ID |"
		<< std::setw(10) << "Key |"
		<< std::setw(12) << "Value |"
		<< "\t"
		<< std::endl;

	std::queue<TreeRecord<typeValue>*> pointerQueue;
	if(treeTable.firstRecord != nullptr)
		pointerQueue.push(treeTable.firstRecord); 

	int counter = 0;
	while (!pointerQueue.empty())
	{
		counter++;
		TreeRecord<typeValue>* temp = pointerQueue.front();
		pointerQueue.pop();

		std::cout << counter
			<< std::setw(10) << temp->getKey()
			<< std::setw(12) << temp->getValue()
			<< "\t"
			<< std::endl;

		if (temp->getPointerToLeft() != nullptr)
			pointerQueue.push(temp->getPointerToLeft());

		if (temp->getPointerToRight() != nullptr)
			pointerQueue.push(temp->getPointerToRight());
	}

	return out;
}
