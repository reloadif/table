#pragma once

// #include <string>
#include <exception>

template <typename typeValue, template<typename typeV> class typeRecord > class AbstractTable;

template <typename typeValue, template<typename typeV> class typeRecord >
class TableRecordArray {
protected:
	int sizeOfTable;
	typeRecord<typeValue>* arrayOfRecords;

public:

	TableRecordArray() = delete;

	TableRecordArray(const int _sizeOfTable);
	TableRecordArray(const TableRecordArray& tableRecordArray);

	~TableRecordArray();

	TableRecordArray& operator = (const TableRecordArray& tableRecordArray);

	typeRecord<typeValue>* getArrayOfRecords();

	friend class AbstractTable<typeValue, typeRecord>;
};


template <typename typeValue, template<typename typeV> class typeRecord >
TableRecordArray<typeValue, typeRecord>::TableRecordArray(const int _sizeOfTable) : sizeOfTable(_sizeOfTable), arrayOfRecords(new typeRecord<typeValue>[sizeOfTable]) {
	if (!arrayOfRecords) {
		throw std::exception("No memory allocated!");
	}
}
template <typename typeValue, template<typename typeV> class typeRecord >
TableRecordArray<typeValue, typeRecord>::TableRecordArray(const TableRecordArray& tableRecordArray) : sizeOfTable(tableRecordArray.sizeOfTable), arrayOfRecords(new typeRecord<typeValue>[sizeOfTable]) {
	if (!arrayOfRecords) {
		throw std::exception("No memory allocated!");
	}
	else {
		for (int i = 0; i < sizeOfTable; ++i)
			arrayOfRecords[i] = tableRecordArray.arrayOfRecords[i];
	}
}


template <typename typeValue, template<typename typeV> class typeRecord >
TableRecordArray<typeValue, typeRecord>& TableRecordArray<typeValue, typeRecord>::operator = (const TableRecordArray& tableRecordArray) {
	if (this == &tableRecordArray) {
		return *this;
	}
	
	if (sizeOfTable != tableRecordArray.sizeOfTable) {
		delete[] arrayOfRecords;

		sizeOfTable = tableRecordArray.sizeOfTable;

		arrayOfRecords = new typeRecord<typeValue>[sizeOfTable];
		if (!arrayOfRecords) {
			throw std::exception("No memory allocated!");
		}
	}

	for (int i = 0; i < sizeOfTable; ++i)
		arrayOfRecords[i] = tableRecordArray.arrayOfRecords[i];

	return *this;
}


template <typename typeValue, template<typename typeV> class typeRecord >
TableRecordArray<typeValue, typeRecord>::~TableRecordArray() {
	if (arrayOfRecords) {
		delete[] arrayOfRecords;
	}
}


template <typename typeValue, template<typename typeV> class typeRecord >
typeRecord<typeValue>* TableRecordArray<typeValue, typeRecord>::getArrayOfRecords() {
	return arrayOfRecords;
}